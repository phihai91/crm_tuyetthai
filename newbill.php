<?php include "php/template/_header.php"; ?>	
<?php include "php/helper/checkuser.php";?>
<?php include "php/template/_sidebar.php"; ?>
<?php include "php/helper/alert.php"; ?>	

	<div class="w3-container" style="margin-left: 25%">
		<h2>
			<span><i class="fa fa-user"></i></span>
			Tìm khách hàng 
		</h2>

		<!-- Search Form -->
		<form action="_customer_search.php" method="get">
			<input class="w3-input w3-border" type="text" name="search" placeholder="Tên hoặc Điện thoại">
			<input type="submit" class="w3-btn-block w3-green w3-margin-top" value="Tìm">

		</form>
		<!--End Search Form -->



		<!-- Result Form -->
		<?php 

		$customerid = null;
		$row = null;

		if(isset($_GET['customerid']))
		{
			$customerid = $_GET['customerid'];
		}

		if (isset($customerid) && $customerid != 0)
		{
			$sql = "
			SELECT * FROM customer WHERE customerid = '$customerid'
			";

			$row = mysqli_query($conn,$sql);
			$row = mysqli_fetch_assoc($row);

		}

		?>

		<!--End Result Form -->


		<!-- check and print customer infomation -->
		<?php if($row)  { ?>
		<table class="w3-table-all w3-hoverable">
				<thead>
					<th>Tên</th>
					<th>Giới tính</th>
					<th>Điện Thoại</th>
					<th>Thành Phố</th>
					<th>Tuổi</th>
				</thead>
				<tr>
					<td><?php echo $row['name']?></td>
					<td><?php echo $row['gender']?></td>
					<td><?php echo $row['mobile']?></td>
					<td><?php echo $row['city']?></td>
					<td><?php echo ($date - $row['birthday']);?></td>
				</tr>
			</table>

		<!-- Form to Next with customeridset -->
		<form action="_newbill_step2.php" method="get">
			<input type="text" name="customerid" value="<?php echo $customerid ?>" hidden>
			<input type="text" name="userid" value="<?php echo $_SESSION['userid']?>" hidden>
			<input type="submit" class="w3-green w3-btn-block w3-margin-top" value="Tiếp tục">
		</form>
		<!--END Form to Next with customeridset -->


		<?php } else { ?>

		<div class="w3-text-orange w3-center"><h3>Không có kết quả trùng khớp <span class="w3-hover-text-green"><a href="#" onclick="showform(event);"><i class="fa fa-plus-square"></i></a></span></div></h3>

		
	</div>
	<!-- Add Customer Form -->
	<form id="addcustomerform" class="w3-padding-top" style="margin-left: 25%" hidden action="_customer_add.php" method="POST">
		<div class="w3-row-padding">
			<div class="w3-third">
				<input class="w3-input w3-border" type="text" placeholder="Họ và tên" required name="name">
			</div>
			<div class="w3-third">
				<input class="w3-input w3-border" type="date" placeholder="Ngày sinh" required name="birthday">
			</div>
			<div class="w3-third">
				<select class="w3-select" name="sex" required>
					<option value="" disabled selected>Giới tính</option>
					<option value="Nam">Nam</option>
					<option value="Nữ">Nữ</option>
				</select>
			</div>
		</div>

		<div class="w3-row-padding w3-padding-top">
			<div class="w3-third">
				<input class="w3-input w3-border" type="text" placeholder="Số điện thoại" required name="phone">  			
			</div>
			<div class="w3-third">
				<input class="w3-input w3-border" type="text" placeholder="Địa chỉ" name="address">  			
			</div>
			<div class="w3-third">
				<select class="w3-select" name="city" required="">
					<option value="" disabled selected>Khu vực</option>
					<option value="HN">Hà Nội</option>
					<option value="OT">Khác</option>
				</select>
			</div>
		</div>
		<div class="w3-container">
			<input type="submit" class="w3-btn-block w3-green w3-margin-top" value="Thêm khách hàng">
		</div>

	</form>
	<!-- End Customer Form -->

		<?php } ?>

	</div>
	
<?php include "php/template/_footer.php"; ?>	

<script>
	function showform(event){
		$("#addcustomerform").fadeToggle(200);
		$("i.fa-plus-square").toggleClass("fa-minus-square");
		
	}

</script>