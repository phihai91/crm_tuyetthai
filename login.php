<?php include "php/template/_header.php"; ?>

<div class="w3-card-4 w3-margin-top " style="width: 33%; margin: 0 auto">
	<form class="w3-container" action="_login.php" method="post">
       	<div class="w3-section">
          <label><b>Username</b></label>
          <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="Enter Username" name="username" required>
          <label><b>Password</b></label>
          <input class="w3-input w3-border" type="password" placeholder="Enter Password" name="password" required>
          <button class="w3-btn-block w3-green w3-section w3-padding" type="submit">Login</button>
          <!-- <input class="w3-check w3-margin-top" type="checkbox" checked="checked" name="rememberme"> Remember me -->
        </div>
      </form>
</div>

<?php include "php/template/_footer.php"; ?>