<?php include "php/template/_header.php"; ?>
<?php include "php/helper/checkadmin.php";?>
<?php include "php/template/_sidebar.php"; ?>	
<?php include "php/helper/alert.php"; ?>

<section class="" style="margin-left: 25%">
	<div class="w3-container w3-green">
		<h3>Thêm dịch vụ mới</h3>
	</div>
	<form class="w3-row-padding w3-container" action="_addservice_add.php" method="post">
		<div class="w3-third w3-margin-top">
			<input type="text" name="name" class="w3-input " placeholder="Tên dịch vụ" required>
		</div>	
		
		<div class="w3-third w3-margin-top">
			<input class="w3-input" type="number" min="0" name="price" placeholder="Giá (Ngàn đồng)" required>
		</div>

		<div class="w3-third w3-margin-top">
			<select class="w3-select" name="type" required>
					<option selected disabled value="">Chuyên Khoa</option>
					<?php 
						//Select Dep Type to Select Form 
						$sql = "SELECT * FROM deptype
								ORDER BY name ASC";
						$deptypelist = mysqli_query($conn,$sql);
						while ($row = mysqli_fetch_assoc($deptypelist)){
							echo "<option>".$row['name']."</option>";
						}
					?>				
			</select>
		</div>

		<div class="w3-twothird w3-margin-top">
			<input class="w3-btn-block w3-light-gray w3-hover-green" type="submit" name="" value="Xác nhận">
		</div>

		<div class="w3-third w3-margin-top">
			<input class="w3-btn-block w3-light-gray w3-hover-red" type="reset" name="" value="Nhập lại ">
		</div>

	</form>

		<!-- 	Service List -->
<?php 
	$sql = "SELECT * FROM service
			ORDER BY name ASC";
	$memberlist = mysqli_query($conn,$sql);
?>
		<section class="w3-card-4">
			<header class="w3-container w3-green">
				<h3>Danh sách tài khoản </h3>
			</header>
			<table class="w3-table-all w3-hoverable">
				<thead>
					<th>Tên</th>
					<th>Giá (Ngàn đồng)</th>
					<th>Chuyên Khoa</th>
				</thead>

					<?php while ($row = mysqli_fetch_assoc($memberlist)) { ?>
							<tr>
								<td>
									<?php echo $row['name']?>
								</td>
								<td>
									<?php echo $row['price']?>
								</td>
								<td>
									<?php echo $row['type']?>
									<span class="w3-right w3-large w3-text-gray w3-hover-text-red"><a onclick="confirmbox(event);" href="_addservice_delete.php?serviceid=<?php echo $row['serviceid'] ?>"><i class="fa fa-minus-square"></a></i></span>
								</td>
							</tr>
					<?php } ?>

			</table>

		</section>



<!-- 	End Service List -->
</section>

<?php include "php/template/_footer.php"; ?>	