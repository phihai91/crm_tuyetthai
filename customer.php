
<?php include "php/template/_header.php"; ?>	
<?php include "php/helper/checkuser.php";?>
<?php include "php/template/_sidebar.php"; ?>
<?php include "php/helper/alert.php"; ?>

	<div class="w3-container" style="margin-left: 25%">
		<h2>
			<span><i class="fa fa-user"></i></span>
			Thêm Thông tin khách hàng 
			<span class="w3-right w3-hover-text-green"><a href="#" onclick="showform(event);"><i class="fa fa-plus-square"></i></a></span>
		</h2>

	</div>
	<!-- Add Customer Form -->
	<form id="addcustomerform" class="w3-padding-top" style="margin-left: 25%" hidden action="_customer_add.php" method="POST">
		<div class="w3-row-padding">
			<div class="w3-third">
	   			<input class="w3-input w3-border" type="text" placeholder="Họ và tên" required name="name">
	  		</div>
	 		<div class="w3-third">
	    		<input class="w3-input w3-border" type="date" placeholder="Ngày sinh" required name="birthday">
	 		</div>
	  		<div class="w3-third">
	    		<select class="w3-select" name="sex" required>
	    			<option value="" disabled selected>Giới tính</option>
	    			<option value="Nam">Nam</option>
	    			<option value="Nữ">Nữ</option>
	    		</select>
	  		</div>
	  	</div>

  		<div class="w3-row-padding w3-padding-top">
  			<div class="w3-third">
    			<input class="w3-input w3-border" type="text" placeholder="Số điện thoại" required name="phone">  			
  			</div>
  			<div class="w3-third">
    			<input class="w3-input w3-border" type="text" placeholder="Địa chỉ" name="address">  			
  			</div>
  			<div class="w3-third">
  				<select class="w3-select" name="city" required="">
	    			<option value="" disabled selected>Khu vực</option>
	    			<option value="HN">Hà Nội</option>
	    			<option value="OT">Khác</option>
	    		</select>
  			</div>
  		</div>
  		<div class="w3-container">
  			<input type="submit" class="w3-btn-block w3-green w3-margin-top" value="Thêm khách hàng">
  		</div>

	</form>
	<!-- End Customer Form -->
	<!-- Customer List -->
<?php 
	$sql = "
			SELECT *, EXTRACT(YEAR FROM birthday) AS birthday
			FROM `customer`
			ORDER BY name ASC
			";
	$memberlist = mysqli_query($conn,$sql);
?>
		<section class="w3-card-4 " style="margin-left: 25%">
			<header class="w3-container w3-green">
				<h3>Danh sách khách hàng </h3>

			</header>
			<table class="w3-table-all w3-hoverable">
				<thead>
					<th>Tên</th>
					<th>Giới tính</th>
					<th>Điện Thoại</th>
					<th>Thành Phố</th>
					<th>Tuổi</th>
					<th></th>
				</thead>

					<?php while ($row = mysqli_fetch_assoc($memberlist)) { ?>
							<tr>
								<td><?php echo $row['name']?></td>
								<td><?php echo $row['gender']?></td>
								<td><?php echo $row['mobile']?></td>
								<td><?php echo $row['city']?></td>
								<td><?php echo ($date - $row['birthday']);?></td>
								<td>
									<span class="w3-right w3-large w3-text-gray w3-hover-text-red"><a onclick="confirmbox(event);" href="_customer_delete.php?customerid=<?php echo $row['customerid'] ?>"><i class="fa fa-minus-square"></a></i></span>
								</td>
							</tr>
					<?php } ?>

			</table>

		</section>



<!-- 	End Customer List -->

<?php include "php/template/_footer.php"; ?>	

<script>
	function showform(event){
		$("#addcustomerform").fadeToggle(200);
		$("i.fa-plus-square").toggleClass("fa-minus-square");
		
	}
</script>