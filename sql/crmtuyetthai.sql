-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 03, 2017 at 12:24 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crmtuyetthai`
--

-- --------------------------------------------------------

--
-- Table structure for table `bill`
--

CREATE TABLE `bill` (
  `billid` int(11) NOT NULL,
  `billidnew` int(11) NOT NULL,
  `customerid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `serviceid` int(11) NOT NULL,
  `createon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bill`
--

INSERT INTO `bill` (`billid`, `billidnew`, `customerid`, `userid`, `serviceid`, `createon`) VALUES
(10, 1, 20, 4, 20, '2017-02-03 14:05:03'),
(11, 11, 21, 4, 21, '2017-02-03 17:22:04'),
(12, 11, 21, 4, 20, '2017-02-03 17:22:04'),
(13, 13, 22, 4, 19, '2017-02-03 18:13:47'),
(14, 13, 22, 4, 21, '2017-02-03 18:13:47'),
(15, 13, 22, 4, 20, '2017-02-03 18:13:47');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `customerid` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_vietnamese_ci NOT NULL,
  `gender` varchar(10) COLLATE utf8_vietnamese_ci NOT NULL,
  `birthday` date DEFAULT NULL,
  `city` varchar(100) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `mobile` varchar(15) COLLATE utf8_vietnamese_ci NOT NULL,
  `address` varchar(256) COLLATE utf8_vietnamese_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customerid`, `name`, `gender`, `birthday`, `city`, `mobile`, `address`) VALUES
(20, 'Phùng Như Mai', 'Nữ', '1994-05-21', 'HN', '01283970803', 'Văn Chương'),
(21, 'Nguyễn Long Phi Hải', 'Nam', '1991-02-22', 'HN', '01273878891', '23 Nguyễn Du, Hai Bà Trưng'),
(22, 'Vũ Nhật Tiến', 'Nam', '1997-11-24', 'HN', '123456', 'Ô Chợ Dừa');

-- --------------------------------------------------------

--
-- Table structure for table `deptype`
--

CREATE TABLE `deptype` (
  `deptypeid` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `deptype`
--

INSERT INTO `deptype` (`deptypeid`, `name`) VALUES
(1, 'Sản'),
(2, 'Răng - Hàm - Mặt'),
(3, 'Nội'),
(4, 'Ngoại'),
(5, 'Khoa Xét Nghiệm'),
(6, 'Khám'),
(7, 'Siêu Âm'),
(8, 'Nội Soi'),
(9, 'Chụp X-Quang'),
(10, 'Điện Tim - Lưu Huyết Não');

-- --------------------------------------------------------

--
-- Table structure for table `doctor`
--

CREATE TABLE `doctor` (
  `doctorid` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_vietnamese_ci NOT NULL,
  `type` varchar(100) COLLATE utf8_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `doctor`
--

INSERT INTO `doctor` (`doctorid`, `name`, `type`) VALUES
(18, 'Duyên', 'Khoa Sản'),
(19, 'BS Thúy', 'Khoa Nội');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `roldid` int(11) NOT NULL,
  `rolename` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`roldid`, `rolename`) VALUES
(1, 'Admin'),
(2, 'User');

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `serviceid` int(11) NOT NULL,
  `name` varchar(100) COLLATE ucs2_vietnamese_ci NOT NULL,
  `type` varchar(100) COLLATE ucs2_vietnamese_ci NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ucs2 COLLATE=ucs2_vietnamese_ci;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`serviceid`, `name`, `type`, `price`) VALUES
(19, 'Khám Nội', 'Điện Tim - Lưu Huyết Não', 20),
(20, 'Khám sản', 'Sản', 120),
(21, 'Khám Ngoại', 'Ngoại', 80);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `userid` int(11) NOT NULL,
  `username` varchar(128) COLLATE ucs2_vietnamese_ci NOT NULL,
  `password` varchar(128) COLLATE ucs2_vietnamese_ci NOT NULL,
  `roleid` int(128) NOT NULL,
  `name` varchar(128) COLLATE ucs2_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ucs2 COLLATE=ucs2_vietnamese_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userid`, `username`, `password`, `roleid`, `name`) VALUES
(4, 'phihai91', 'e2411fd01be3e5cdcf10b8b97d7bcf63', 1, 'Hai Nguyen'),
(5, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, 'admin'),
(19, 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 2, 'user'),
(20, 'nhumai94', 'b3812221cd38255b0f2c39e15e04e740', 1, 'Mai Phung');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bill`
--
ALTER TABLE `bill`
  ADD PRIMARY KEY (`billid`),
  ADD KEY `customerid` (`customerid`,`userid`,`serviceid`),
  ADD KEY `fk_bill_userid_user` (`userid`),
  ADD KEY `fk_bill_serviceid_service` (`serviceid`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customerid`);

--
-- Indexes for table `deptype`
--
ALTER TABLE `deptype`
  ADD PRIMARY KEY (`deptypeid`);

--
-- Indexes for table `doctor`
--
ALTER TABLE `doctor`
  ADD PRIMARY KEY (`doctorid`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`roldid`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`serviceid`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userid`),
  ADD KEY `type` (`roleid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bill`
--
ALTER TABLE `bill`
  MODIFY `billid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `customerid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `deptype`
--
ALTER TABLE `deptype`
  MODIFY `deptypeid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `doctor`
--
ALTER TABLE `doctor`
  MODIFY `doctorid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `serviceid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_user_role_type_roleid` FOREIGN KEY (`roleid`) REFERENCES `role` (`roldid`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
