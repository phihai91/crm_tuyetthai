
<?php include "php/template/_header.php"; ?>	
<?php include "php/helper/checkuser.php";?>
<?php include "php/template/_sidebar.php"; ?>
<?php include "php/helper/alert.php"; ?>

	
	<!-- Customer List -->
<?php 
	$sql = "
			SELECT b.createon, c.name, u.username, SUM(s.price) AS p
			FROM bill AS b
			LEFT JOIN customer AS c ON b.customerid = c.customerid 
			LEFT JOIN user AS u ON u.userid = b.userid
			LEFT JOIN service AS s ON b.serviceid = s.serviceid
			GROUP BY billidnew
			";
	$billlist = mysqli_query($conn,$sql);
?>
		<section class="w3-card-4 " style="margin-left: 25%">
			<header class="w3-container w3-green">
				<h3>Danh sách hoá đơn </h3>

			</header>
			<table class="w3-table-all w3-hoverable">
				<thead>
					<th>Ngày tạo</th>
					<th>Tên khách hàng</th>
					<th>Thành tiền</th>
					<th>Thu ngân</th>
					<th></th>
				</thead>

					<?php while ($row = mysqli_fetch_assoc($billlist)) { ?>
							<tr>
								<td><?php echo $row['createon']?></td>
								<td><?php echo $row['name']?></td>
								<td><?php echo $row['p']?></td>
								<td><?php echo $row['username']?></td>


								<td>
									<span class="w3-right w3-large w3-text-gray w3-hover-text-red"><a onclick="confirmbox(event);" href=""><i class="fa fa-minus-square"></a></i></span>
								</td>
							</tr>
					<?php } ?>

			</table>

		</section>



<!-- 	End Customer List -->

<?php include "php/template/_footer.php"; ?>	

<script>
	function showform(event){
		$("#addcustomerform").fadeToggle(200);
		$("i.fa-plus-square").toggleClass("fa-minus-square");
		
	}
</script>