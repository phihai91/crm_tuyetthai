<?php include "php/template/_header.php"; ?>
<?php include "php/helper/checkadmin.php";?>
<?php include "php/template/_sidebar.php"; ?>
<?php include "php/helper/alert.php"; ?>

<?php
	// define variables and set to empty values
	$userid = null;

	if ($_SERVER["REQUEST_METHOD"] == "GET") {
		$userid = $_GET["userid"];
	}

	$sql = "
	SELECT * FROM user WHERE userid = '$userid'
	";

	$result = mysqli_query($conn,$sql);
	$userinfo = mysqli_fetch_assoc($result);
?>

<div style="margin-left: 25%" class="w3-container">
	<h3>Thay đổi thông tin tài khoản: <b><?php echo $userinfo['username']?></b></h3>
	<form method="POST" action="_admin_editrole_complete.php">
		<input type="text" name="userid" value="<?php echo $userid ?>" hidden>
		
		<select class="w3-select" name="type">
				<option selected value="">Bộ phận</option>	
				<option value="2">User</option>	
				<option value="1">Admin</option>	
			</select>
		<p>
			<input class="w3-input w3-border w3-round" type="text" name="username" value="<?php echo $userinfo['username'] ?>" required>
		</p>
		<p>
			<input class="w3-input w3-border w3-round" type="text" name="name" value="<?php echo $userinfo['name'] ?>" required>
		</p>
		<p>
			<input class="w3-input w3-border w3-round" type="password" name="password" placeholder="" value="">
		</p>
		<p>
			<input class="w3-btn-block w3-green" type="submit" value="Sửa tài khoản" onclick="confirmbox(event)">
		</p>
	</form>
</div>

<?php include "php/template/_footer.php"; ?>