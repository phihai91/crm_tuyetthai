<?php include "php/template/_header.php"; ?>	
<?php include "php/helper/checkadmin.php";?>
<?php include "php/template/_sidebar.php"; ?>
<?php include "php/helper/alert.php"; ?>


<section style="margin-left: 25%">
	<!-- Add member -->
	<div id="alertpass" class="w3-panel w3-orange w3-text-white" hidden>
	<span onclick="$(this).parent().hide(1000)" class="w3-closebtn">&times;</span>
		<h3>Password nhập không giống nhau !!</h3>
		<p>Hãy nhập lại Password</p>
	</div>
	<div class="w3-green w3-container w3-padding-4">
		<h3>Thêm User</h3>
	</div> 
	<form class="w3-row-padding w3-margin-top" onsubmit="return checkrepass()" method="post" action="_admin_addrole.php">
		<div class="w3-third">
			<input class="w3-input w3-border" type="text" name="username"  placeholder="Username" required>
		</div>
		<div class="w3-third">
			<input class="w3-input w3-border" id="pass1" type="password" name="password"  placeholder="Password" required>
		</div>
		<div class="w3-third">
			<input class="w3-input w3-border" id="pass2" type="password" name="password2"  placeholder="Re-enter Password" required>
		</div>

		<div class="w3-twothird w3-margin-top">
			<input class="w3-input w3-border" type="text" name="name" placeholder="Họ và tên">
		</div>

		<div class="w3-third w3-margin-top">
			<select class="w3-select" required name="type">
				<option disabled selected value="">Bộ phận</option>	
				<option value="2">User</option>	
				<option value="1">Admin</option>	
			</select>
		</div>

		<div class="w3-twothird w3-margin-top">
			<input class="w3-btn-block w3-light-gray w3-hover-green" type="submit" name="" value="Xác nhận">
		</div>

		<div class="w3-third w3-margin-top">
			<input class="w3-btn-block w3-light-gray w3-hover-red" type="reset" name="" value="Nhập lại">
		</div>
		</div>

	</form>
		<!-- End Add member -->


	<!-- 	Member List -->
<?php 
	//$sql = "SELECT * FROM user";
	$sql = "
	SELECT user.username, role.rolename, user.name, user.userid
	FROM `user` 
	left join role
	ON user.roleid = role.roldid
	";
	$memberlist = mysqli_query($conn,$sql);
?>
		<section class="w3-card-4">
			<header class="w3-container w3-green">
				<h3>Danh sách tài khoản </h3>
			</header>
			<table class="w3-table-all w3-hoverable">
				<thead>
					<th>Username</th>
					<th>Họ và tên</th>
					<th>Nhóm</th>
				</thead>

					<?php while ($row = mysqli_fetch_assoc($memberlist)) { ?>
							<tr>
								<td>
									<?php echo $row['username']?>
								</td>
								<td>
									<?php echo $row['name']?>
								</td>
								<td>
									<?php echo $row['rolename']?>

									<!-- Icon Delete -->
									<span class="w3-right w3-large w3-text-gray w3-hover-text-red"><a href="_admin_deleterole.php?userid=<?php echo $row['userid'] ?>" onclick="confirmbox(event);"><i class="fa fa-minus-square"></a></i></span>

									<!-- Icon Edit -->
									<span class="w3-right w3-large w3-text-gray w3-hover-text-red w3-padding-right"><a href="_admin_editrole.php?userid=<?php echo $row['userid'] ?>"><i class="fa fa-pencil"></a></i></span>
								</td>
							</tr>
					<?php } ?>

			</table>

		</section>



<!-- 	End Doctor List -->

</section>


<?php include "php/template/_footer.php"; ?>	
<script>
	function checkrepass()
	{
		var pass1 = document.getElementById("pass1").value;
		var pass2 = document.getElementById("pass2").value;
		var ok = true;
		if (pass1 != pass2) 
		{
			$("#alertpass").fadeIn(1000);
        	ok = false;
   		}
    return ok;
	}

</script>