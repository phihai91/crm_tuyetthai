<?php include "php/template/_header.php"; ?>	
<?php include "php/helper/checkadmin.php";?>
<?php include "php/template/_sidebar.php"; ?>
<?php include "php/helper/alert.php"; ?>

	
 <!-- 	Add Doctor --> 	
	<form class="" style="margin-left: 25%" action="_adddoctor_add.php" method="get">
		<header class="w3-green w3-container">
			<div class="w3-left"><h3>Thêm thông tin bác sĩ</h3></div>
			<div class="w3-right w3-hover-text-indigo"><h3><a href="#" onclick="hideform(event);">Hide</a></h3></div>
		</header>
		<div class="w3-row-padding w3-margin-top">
			<div class="w3-twothird">
					<input type="text" class="w3-input w3-border" name="name" placeholder="Tên bác sĩ" required>
			</div>
			<div class="w3-container w3-third">
				<select class="w3-select" name="type" required>
					<option selected disabled value="">Chuyên Khoa</option>
					<?php 
						//Select Dep Type to Select Form 
						$sql = "SELECT * FROM deptype";
						
						$deptypelist = mysqli_query($conn,$sql);

						while ($row = mysqli_fetch_assoc($deptypelist)){
							echo "<option>".$row['name']."</option>";

						}
					?>				
			</select>
			</div>
			<div class="w3-twothird w3-margin-top">
				<input type="submit" class="w3-btn-block w3-light-gray w3-hover-green" value="Xác nhận">
			</div>
			<div class="w3-third w3-margin-top">
				<input type="reset" name="type" class="w3-btn-block w3-light-gray w3-hover-red" value="Nhập lại">
			</div>
		</div>
	</form>
<!-- 	End Add Doctor -->

<!-- 	Doctor List -->
<?php 
	$sql = "SELECT * FROM doctor";

	$doctorlist = mysqli_query($conn,$sql);

	//pagination

	$currentPage = 0;

	if(isset($_GET["page"])) {
        
        $currentPage = $_GET["page"];
    }

	$noOfItems = 10;
	$totalItems = mysqli_num_rows($doctorlist);
	$noOfPages = $totalItems / $noOfItems;

	$userpage = $currentPage * $noOfItems;

	$doctorlist = mysqli_fetch_all($doctorlist,MYSQLI_ASSOC); //return the result-set as an associative array:

	$doctorlist = array_slice($doctorlist, $userpage,$noOfItems);

?>
		<section class="w3-card-4" style="margin-left: 25%">
			<header class="w3-container w3-green">
				<h3>Danh sách bác sĩ </h3>
			</header>
			<table class="w3-table-all w3-hoverable">
				<thead>
					<th>Họ và Tên</th>
					<th>Chuyên Khoa</th>
				</thead>

					<?php foreach ($doctorlist as $row){ ?>
							<tr>
								<td>
									<?php echo $row['name']?>
								</td>
								<td>
									<?php echo $row['type']?>
									<span class="w3-right w3-large w3-text-gray w3-hover-text-red"><a onclick="confirmbox(event);" href="_adddoctor_delete.php?doctorid=<?php echo $row["doctorid"] ?>"><i class="fa fa-minus-square"></a></i></span>
								</td>
							</tr>
					<?php } ?>

			</table>

			<!-- pagination bar -->
			<div class="w3-center w3-margin-top">
				<ul class="w3-pagination w3-border w3-round w3-center">
					<li><a href="adddoctor.php">&laquo;</a></li>

					<?php
						for ($i=0; $i < $noOfPages; $i++) 
						{ 
							$url = "adddoctor.php?page=".$i;
							echo '<li><a class="" href="'.$url.'">'.($i+1).'</a></li>';
						}
					?>

					<li><a href="adddoctor.php?page=<?php echo round($noOfPages) ?>">&raquo;</a></li>
				</ul>
			</div>
			<!-- end pagination -->

		</section>



<!-- 	End Doctor List -->

<?php include "php/template/_footer.php"; ?>	
<!-- script -->
<script>
	function hideform(event){
		$("form").toggle(1000);
	}	

</script>
<!-- End script -->