	<!-- Alert -->
	<?php 

		// Show success or not 
		$status = null;
		if (isset($_GET["status"]))
		{
			$status = $_GET["status"];
		}

	?>
	<div id="alert">
		<?php if ($status == 1) { ?>
		<div class="w3-panel w3-pale-yellow w3-leftbar w3-card-4 w3-border-amber" style="margin-left: 25%">
	    	<span onclick="this.parentElement.style.display='none'" class="w3-closebtn">&times;</span>
	    	<h3>Thành công</h3>
	  	</div>

	  	<?php } else if ($status == 2) { ?>
		<div class="w3-panel w3-red w3-card-4" style="margin-left: 25%">
	    	<span onclick="this.parentElement.style.display='none'" class="w3-closebtn">&times;</span>
	    	<h3>Thao tác không thành công</h3>  		
	    </div>
  		<?php } else if ($status == 3) { ?>
		<div class="w3-panel w3-red w3-card-4" style="margin-left: 25%">
	    	<span onclick="this.parentElement.style.display='none'" class="w3-closebtn">&times;</span>
	    	<h3>Tài khoản đã tồn tại</h3>  		
	    </div>
  		<?php } ?>

  	</div>
 <!-- End Alert -->

 <script>
 	function confirmbox(event){
		var r = confirm("Bạn có chắc chắn thao tác này  ?");
		if (r == false) {
			event.preventDefault();
		}
	}
 </script>