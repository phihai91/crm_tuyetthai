<?php include "php/helper/utility.php" ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>CRM Tuyet Thai Company</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="css/w3.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/mystyle.css">
 <!--  <link rel="stylesheet" href="plugin/bootstrap/css/bootstrap.min.css">
  <script src="plugin/bootstrap/js/jquery.js"></script>
  <script src="plugin/bootstrap/js/bootstrap.min.js"></script> -->

</head>
<body>
<!-- Dimscreen -->
<div style=" position: fixed; z-index:1002;" hidden=""><button class="w3-btn w3-green" onclick="dim();">click me</button></div>
<div id="dim" style="
    background:#000;
    opacity:0.5;
    position:fixed; /* important to use fixed, not absolute */
    top:0;
    left:0;
    width:100%;
    height:100%;
    z-index:1000; /* may not be necessary */
" hidden=""></div>

<!-- End Dimscreen -->
<header class="w3-container w3-border ">
	<div class="w3-left w3-padding-16">
		<a href="index.php"><img class="" height="50em" src="imgs/logo.png"></a>
	</div>
	<ul class="w3-navbar w3-right w3-padding-16 w3-large">
  <?php if (isset($_SESSION['name'])) { ?>

    <li class="w3-padding-8"><b>Xin chào</b> <?php echo $_SESSION['name'] ?></li>
		<li><a href="_login_logout.php" class="w3-hover-green w3-round">Thoát</a></li>
	</ul>
  <?php } else { ?>
   <li><a href="login.php" class="w3-hover-green w3-round">Đăng Nhập</a></li>
  <?php } ?>

</header>